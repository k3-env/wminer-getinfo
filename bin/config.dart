import 'dart:convert';
import 'dart:io';
import 'package:json_annotation/json_annotation.dart';
import 'asic/asic.dart';

part 'config.g.dart';

@JsonSerializable(createToJson: false)
class Config {
  final Asic asic;
  final InfluxConfig influxdb;
  final Global global;

  Config(this.asic, this.influxdb, this.global);

  factory Config.fromJson(Map<String, dynamic> json) => _$ConfigFromJson(json);

  factory Config.load({String path = "config.json"}) =>
      _$ConfigFromJson(jsonDecode(File('./config.json').readAsStringSync()));
}

@JsonSerializable(createToJson: false)
class Global {
  final int interval;

  Global(this.interval);

  factory Global.fromJson(Map<String, dynamic> json) => _$GlobalFromJson(json);
}

@JsonSerializable(createToJson: false)
class InfluxConfig {
  final String url;
  final String token;
  final String org;
  final String bucket;
  final bool debugEnabled;

  InfluxConfig(this.url, this.token, this.org, this.bucket,
      {this.debugEnabled = false});

  factory InfluxConfig.fromJson(Map<String, dynamic> json) =>
      _$InfluxConfigFromJson(json);
}
