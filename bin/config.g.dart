// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Config _$ConfigFromJson(Map<String, dynamic> json) => Config(
      Asic.fromJson(json['asic'] as Map<String, dynamic>),
      InfluxConfig.fromJson(json['influxdb'] as Map<String, dynamic>),
      Global.fromJson(json['global'] as Map<String, dynamic>),
    );

Global _$GlobalFromJson(Map<String, dynamic> json) => Global(
      json['interval'] as int,
    );

InfluxConfig _$InfluxConfigFromJson(Map<String, dynamic> json) => InfluxConfig(
      json['url'] as String,
      json['token'] as String,
      json['org'] as String,
      json['bucket'] as String,
      debugEnabled: json['debugEnabled'] as bool? ?? false,
    );
