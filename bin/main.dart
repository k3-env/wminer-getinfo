import 'dart:async';
import 'dart:io';

import 'package:influxdb_client/api.dart';

import 'asic/asic_request.dart';
import 'config.dart';

void main() async {
  int packet = 0;

  Config config = Config.load(path: "./config.json");
  final AsicRequest req = AsicRequest(config.asic);

  InfluxDBClient client = InfluxDBClient(
      url: config.influxdb.url,
      token: config.influxdb.token,
      org: config.influxdb.org,
      bucket: config.influxdb.bucket,
      debug: config.influxdb.debugEnabled);
  WriteService _service = WriteService(client);
  ProcessSignal.sigint.watch().listen((event) {
    print("");
    print("request to termination");
    exit(0);
  });
  Timer.periodic(Duration(milliseconds: config.global.interval), (t) {
    try {
      req.getData((summary) {
        _service.write(config.asic.applyLabels(summary.point));
        packet++;
        stdout.writeln("sent packet #$packet");
      }, _writeError);

      req.getBoardsInfo((boards) {
        for (var board in boards) {
          _service.write(config.asic.applyLabels(board.point));
          packet++;
          stdout.writeln("sent packet #$packet");
        }
      }, _writeError);

      req.getPoolInfo((pools) {
        for (var pool in pools) {
          _service.write(config.asic.applyLabels(pool.point));
          packet++;
          stdout.writeln("sent packet #$packet");
        }
      }, _writeError);
    } catch (e, st) {
      _writeError(e, st);
    }
  });
}

void _writeError(Object object, StackTrace stackTrace) {
  stderr.writeln(object);
  stderr.writeln(stackTrace);
}
