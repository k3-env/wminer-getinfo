import 'package:influxdb_client/api.dart';
import 'package:json_annotation/json_annotation.dart';

part 'asic.g.dart';

@JsonSerializable(createToJson: false)
class Asic {
  @JsonKey(name: 'address')
  final String ip;
  final int port;
  late final Map<String, String> _labels;

  Map<String, String> get labels => _labels;

  void addLabel(String label, String value) => _labels[label] = value;

  Asic(this.ip, this.port) : _labels = {};

  factory Asic.fromJson(Map<String, dynamic> json) => _$AsicFromJson(json);

  Point applyLabels(Point point) {
    var pt = point;
    pt.addTag('ip', ip);
    for (var label in _labels.entries) {
      pt.addTag(label.key, label.value);
    }
    return pt;
  }
}
