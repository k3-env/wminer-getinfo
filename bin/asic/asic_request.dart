import 'dart:convert';
import 'dart:io' show Socket;
import 'dart:typed_data';
import 'asic.dart';
import 'asic_response.dart';

class AsicRequest {
  final Asic _asic;

  String get ip => _asic.ip;

  AsicRequest(this._asic);

  void getData(void Function(AsicSummaryResponse) onSuccess,
      void Function(Object, StackTrace)? onError) async {
    _getRawData('summary', (json) {
      onSuccess(json.summary!.first);
    }, (e, st) => onError!(e, st));
  }

  void getPoolInfo(void Function(List<AsicPoolResponse>) onSuccess,
      void Function(Object, StackTrace)? onError) async {
    _getRawData('pools', (json) {
      onSuccess(json.pools!);
    }, (e, st) => onError!(e, st));
  }

  void _getRawData(String request, void Function(AsicRawResponse) onSuccess,
      void Function(Object, StackTrace)? onError) async {
    try {
      Socket socket = await Socket.connect(_asic.ip, _asic.port);
      socket.listen((Uint8List data) {
        try {
          String text = String.fromCharCodes(data);
          var response = AsicRawResponse.fromJson(jsonDecode(text));
          onSuccess(response);
          socket.close();
        } catch (e, st) {
          socket.destroy();
          onError!(e, st);
        }
      }, onError: onError);
      var cmd = {'cmd': request.toLowerCase()};
      socket.write(jsonEncode(cmd));
    } catch (e, st) {
      onError!(e, st);
    }
  }

  void getBoardsInfo(void Function(List<AsicBoardResponse>) onSuccess,
      void Function(Object, StackTrace)? onError) async {
    _getRawData('devs', (json) {
      onSuccess(json.boards!);
    }, (e, st) => onError!(e, st));
  }
}
