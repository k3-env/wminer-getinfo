// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'asic_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AsicStatus _$AsicStatusFromJson(Map<String, dynamic> json) => AsicStatus(
      json['STATUS'] as String,
      json['Code'] as int,
      json['Description'] as String,
      json['Msg'] as String,
      json['When'] as int,
    );

AsicRawResponse _$AsicRawResponseFromJson(Map<String, dynamic> json) =>
    AsicRawResponse(
      (json['STATUS'] as List<dynamic>)
          .map((e) => AsicStatus.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['POOLS'] as List<dynamic>?)
          ?.map((e) => AsicPoolResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['DEVS'] as List<dynamic>?)
          ?.map((e) => AsicBoardResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['SUMMARY'] as List<dynamic>?)
          ?.map((e) => AsicSummaryResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

AsicSummaryResponse _$AsicSummaryResponseFromJson(Map<String, dynamic> json) =>
    AsicSummaryResponse(
      json['MAC'] as String,
      (json['HS RT'] as num).toDouble(),
      (json['Temperature'] as num).toDouble(),
      (json['Env Temp'] as num).toDouble(),
      json['Power'] as int,
      json['Fan Speed In'] as int,
      json['Fan Speed Out'] as int,
      json['Uptime'] as int,
    );

AsicPoolResponse _$AsicPoolResponseFromJson(Map<String, dynamic> json) =>
    AsicPoolResponse(
      json['POOL'] as int,
      json['URL'] as String,
      json['Status'] as String,
      json['User'] as String,
      json['Getworks'] as int,
      json['Accepted'] as int,
      json['Rejected'] as int,
      json['Discarded'] as int,
    );

AsicBoardResponse _$AsicBoardResponseFromJson(Map<String, dynamic> json) =>
    AsicBoardResponse(
      json['ID'] as int,
      json['Slot'] as int,
      json['Name'] as String,
      AsicBoardResponse._enabledFromJson(json['Enabled'] as String),
      json['PCB SN'] as String,
      json['Status'] as String,
      (json['Temperature'] as num).toDouble(),
      (json['Chip Frequency'] as num).toDouble(),
      (json['MHS 5s'] as num).toDouble(),
      json['Accepted'] as int,
      json['Rejected'] as int,
      json['Hardware Errors'] as int,
      (json['Chip Temp Avg'] as num).toDouble(),
      (json['Chip Temp Min'] as num).toDouble(),
      (json['Chip Temp Max'] as num).toDouble(),
    );
