import 'package:influxdb_client/api.dart';
import 'package:json_annotation/json_annotation.dart';

part 'asic_response.g.dart';

@JsonSerializable(createToJson: false)
class AsicStatus {
  @JsonKey(name: 'STATUS')
  final String status;

  @JsonKey(name: 'When')
  final int when;

  @JsonKey(name: 'Code')
  final int code;

  @JsonKey(name: 'Msg')
  final String message;

  @JsonKey(name: 'Description')
  final String description;

  AsicStatus(this.status, this.code, this.description, this.message, this.when);

  factory AsicStatus.fromJson(Map<String, dynamic> json) =>
      _$AsicStatusFromJson(json);
}

@JsonSerializable(createToJson: false)
class AsicRawResponse {
  @JsonKey(name: 'STATUS')
  final List<AsicStatus> status;

  @JsonKey(name: 'POOLS')
  final List<AsicPoolResponse>? pools;

  @JsonKey(name: 'DEVS')
  final List<AsicBoardResponse>? boards;

  @JsonKey(name: 'SUMMARY')
  final List<AsicSummaryResponse>? summary;

  AsicRawResponse(this.status, this.pools, this.boards, this.summary);

  factory AsicRawResponse.fromJson(Map<String, dynamic> json) =>
      _$AsicRawResponseFromJson(json);
}

@JsonSerializable(createToJson: false)
class AsicSummaryResponse {
  @JsonKey(name: 'MAC')
  final String macAddr;

  @JsonKey(name: 'HS RT')
  final double hashrate;
  @JsonKey(name: 'Temperature')
  final double tempHardware;
  @JsonKey(name: 'Env Temp')
  final double tempEnvironment;
  @JsonKey(name: 'Power')
  final int power;
  @JsonKey(name: 'Fan Speed In')
  final int fanSpeedIn;
  @JsonKey(name: 'Fan Speed Out')
  final int fanSpeedOut;
  @JsonKey(name: 'Uptime')
  final int uptime;

  AsicSummaryResponse(
      this.macAddr,
      this.hashrate,
      this.tempHardware,
      this.tempEnvironment,
      this.power,
      this.fanSpeedIn,
      this.fanSpeedOut,
      this.uptime);

  Point get point => Point('asic')
      .addTag('mac', macAddr)
      .addField('tempHard', tempHardware)
      .addField('power', power)
      .addField('tempEnv', tempEnvironment)
      .addField('hashrateAvg', hashrate)
      .addField('uptime', uptime)
      .addField('fanIn', fanSpeedIn)
      .addField('fanOut', fanSpeedOut);

  factory AsicSummaryResponse.fromJson(Map<String, dynamic> json) =>
      _$AsicSummaryResponseFromJson(json);
}

@JsonSerializable(createToJson: false)
class AsicPoolResponse {
  @JsonKey(name: 'POOL')
  final int poolId;
  @JsonKey(name: 'URL')
  final String url;
  @JsonKey(name: 'User')
  final String user;

  @JsonKey(name: 'Status')
  final String status;
  @JsonKey(name: 'Getworks')
  final int getworks;
  @JsonKey(name: 'Accepted')
  final int accepted;
  @JsonKey(name: 'Rejected')
  final int rejected;
  @JsonKey(name: 'Discarded')
  final int discarded;

  Point get point => Point('asic-pool')
      .addTag('poolId', "pool-$poolId")
      .addTag('url', url)
      .addTag('user', user)
      .addField('status', status)
      .addField('getworks', getworks)
      .addField('accepted', accepted)
      .addField('rejected', rejected)
      .addField('discarded', discarded);

  AsicPoolResponse(this.poolId, this.url, this.status, this.user, this.getworks,
      this.accepted, this.rejected, this.discarded);

  factory AsicPoolResponse.fromJson(Map<String, dynamic> json) =>
      _$AsicPoolResponseFromJson(json);
}

@JsonSerializable(createToJson: false)
class AsicBoardResponse {
  @JsonKey(name: 'ID')
  final int id;
  @JsonKey(name: 'Slot')
  final int slot;
  @JsonKey(name: 'Name')
  final String name;
  @JsonKey(name: 'Enabled', fromJson: (_enabledFromJson))
  final bool enabled;
  @JsonKey(name: 'PCB SN')
  final String serial;

  @JsonKey(name: 'Status')
  final String status;
  @JsonKey(name: 'Temperature')
  final double temperature;
  @JsonKey(name: 'Chip Frequency')
  final double frequency;
  @JsonKey(name: 'MHS 5s')
  final double hashrate;
  @JsonKey(name: 'Accepted')
  final int accepted;
  @JsonKey(name: 'Rejected')
  final int rejected;
  @JsonKey(name: 'Hardware Errors')
  final int hwErrors;
  @JsonKey(name: 'Chip Temp Avg')
  final double chipTempAvg;
  @JsonKey(name: 'Chip Temp Min')
  final double chipTempMin;
  @JsonKey(name: 'Chip Temp Max')
  final double chipTempMax;

  static bool _enabledFromJson(String input) {
    return input == 'Y';
  }

  Point get point => Point('asic-board')
      .addTag('boardId', "board-$id")
      .addTag('name', name)
      .addTag('serial', serial)
      .addTag('slot', "$slot")
      .addField('status', status)
      .addField('boardTemperature', temperature)
      .addField('frequency', frequency)
      .addField('hashrateAvg', hashrate)
      .addField('accepted', accepted)
      .addField('rejected', rejected)
      .addField('hwErrors', hwErrors)
      .addField('chipTemperatureMin', chipTempMin)
      .addField('chipTemperatureMax', chipTempMax)
      .addField('chipTemperatureAvg', chipTempAvg);

  AsicBoardResponse(
      this.id,
      this.slot,
      this.name,
      this.enabled,
      this.serial,
      this.status,
      this.temperature,
      this.frequency,
      this.hashrate,
      this.accepted,
      this.rejected,
      this.hwErrors,
      this.chipTempAvg,
      this.chipTempMin,
      this.chipTempMax);

  factory AsicBoardResponse.fromJson(Map<String, dynamic> json) =>
      _$AsicBoardResponseFromJson(json);
}
